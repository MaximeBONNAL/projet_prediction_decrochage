# -*- coding: utf-8 -*-

from os import path

class DataParser():
    """
    Parser of the csv files from the OULA dataset.
    
    
    Attributes
    ----------
    rep_path : str
        The path to the directory where are located the data.
        
    csv_names : list of str
        The list of csv files to parse.
        
    final_results : list of list of str
        List of the final results of each student to different courses. 
        Follow the format [code_module, code_presentation, id_student, final_result].
        
    scores : list of list of str
        List of the scores of each student to different assessments.
        Follow the format [id_assessment, id_student, score].
    
    assessments_types : list of list of str
        List of the types of assessments of each assessment of different
        presentations of modules.
        Follow the format [code_module, code_presentation, id_assessment, assessment_type].
        
    modules_presentations_lengths : list of list of str
        List of the presentations lengths of each module.
        Follow the format [code_module, code_presentation, module_presentation_length]
    """
    
    
    def __init__(self, rep_path):
        """Constructor of the DataParser class.
    
        Constructor of the DataParser class needing `rep_path`, a string 
        corresponding to the path to the data directory.

        Parameters
        ----------
        rep_path : str
            The path to the directory where are located the data.

        Returns
        -------
        None.

        """
        self.rep_path = rep_path if rep_path[-1] == '/' else rep_path + '/'
        self.csv_names = ["studentInfo.csv", "studentAssessment.csv", 
                          "assessments.csv", "courses.csv"]
        self.final_results = []
        self.scores = []
        self.assessments_types = []
        self.modules_presentations_lengths = []
        
    def parse(self):
        """Parse the data from relevant csv files.
        
        Data are parsed under the condition that the `rep_path` attribute
        match with the directory containing the csv files from OULA. 
        

        Raises
        ------
        IOError
            If the file can't be open.

        Returns
        -------
        None.

        """
        # Check if the repository containing the csv files exists and 
        # has all the csv files.
        if(self.existAndIsDir() and self.dirContainsCsvFiles()):
            # Open successively all the csv files.
            for csv_name in self.csv_names:
                full_path = self.rep_path + csv_name
                
                try:
                    with open(full_path, "r") as file:
                        # Initializing field indexes 
                        if csv_name == "studentInfo.csv":
                            indexes = [None] * 4
                        elif csv_name == "studentAssessment.csv":
                            indexes = [None] * 3
                        elif csv_name == "assessments.csv":
                            indexes = [None] * 4
                        elif csv_name == "courses.csv":
                            indexes = [None] * 3
                        
                        # Split each line field.
                        for count_line, line in enumerate(file):
                            list_line = line.strip().replace("\"","").split(",")
                            # Handle fields or values from the different csv files.
                            if count_line == 0:
                                if csv_name == "studentInfo.csv":
                                    indexes = self.getStudentInfoIndexes(list_line, indexes)
                                elif csv_name == "studentAssessment.csv":
                                    indexes = self.getStudentAssessmentIndexes(list_line, indexes)
                                elif csv_name == "assessments.csv":
                                    indexes = self.getAssessmentsIndexes(list_line, indexes)
                                elif csv_name == "courses.csv":
                                    indexes = self.getCoursesIndexes(list_line, indexes)
                            else:
                                if csv_name == "studentInfo.csv":
                                    self.addStudentInfoValues(list_line, indexes)
                                    #print(self.final_results)

                                elif csv_name == "studentAssessment.csv":
                                    self.addStudentAssessmentValues(list_line, indexes)
                                elif csv_name == "assessments.csv":
                                    self.addAssessmentsValues(list_line, indexes)
                                elif csv_name == "courses.csv":
                                    self.addCoursesValues(list_line, indexes)
                except IOError:
                    raise IOError("Error : Could not open the file " + full_path)
                                    
                                        
                                    
    
    def existAndIsDir(self):
        """Check if the path exists and is a directory.
        
        Check if the path from the `rep_path` attribute exists and 
        is a directory. 
        

        Returns
        -------
        res : boolean
            True if the path exists and is a directory. False otherwise.

        """
        res = False
        if(path.exists(self.rep_path)):
            if(path.isdir(self.rep_path)):
                print(self.rep_path + " exist and is a directory.")
                res = True
            else:
                print(self.rep_path + " exist but is not a directory. A directory is expected.")
        else:
            print(self.rep_path + " doesn't exist.")
        return res
    
    
    def dirContainsCsvFiles(self):
        """Check if the directory contains the required csv files.
        
        Check if the path from the `rep_path` attribute contains the 
        required csv files stored in the `csv_names` attribute.
        

        Returns
        -------
        res : boolean
            True if all the files are in the directory. False otherwise.

        """
        res = True
        for csv_name in self.csv_names:
            full_path = self.rep_path + csv_name
            if(path.exists(full_path)):
                print(full_path + " exist.")
            else:
                print(full_path + " doesn't exist. " + self.rep_path + " should contain " + csv_name)
                res = False
        return res
        
    
    def getStudentInfoIndexes(self, list_line, indexes):
        """Get the indexes of the relevant field from studentInfo.csv.
        
        Get the indexes of the relevant field from studentInfo.csv.
        The indexes are stored in the `indexes` variable.
        

        Parameters
        ----------
        list_line : list of str
            List of every field of the studentInfo.csv.
        indexes : list of int
            List of indexes of every relevant field from studentInfo.csv.
            Follow the format 
            [code_module_index, code_presentation_index,
             id_student_index, final_result_index]

        Returns
        -------
        None.

        """
        for count_field, field in enumerate(list_line):
            if field == "code_module":
                indexes[0] = count_field
            elif field == "code_presentation":
                indexes[1] = count_field
            elif field == "id_student":
                indexes[2] = count_field
            elif field == "final_result":
                indexes[3] = count_field
        return indexes
                
    def getStudentAssessmentIndexes(self, list_line, indexes):
        """Get the indexes of the relevant field from studentAssessment.csv.
        
        Get the indexes of the relevant field from studentAssessment.csv.
        The indexes are stored in the `indexes` variable.

        Parameters
        ----------
        list_line : list of str
            List of every field of the studentAssessment.csv.
        indexes : list of int
            List of indexes of every relevant field from studentAssessment.csv.
            Follow the format 
            [id_assessment_index, id_student_index, score_index]

        Returns
        -------
        None.

        """
        for count_field, field in enumerate(list_line):
            if field == "id_assessment":
                indexes[0] = count_field
            elif field == "id_student":
                indexes[1] = count_field
            elif field == "score":
                indexes[2] = count_field
        return indexes
                
                
    def getAssessmentsIndexes(self, list_line, indexes):
        """Get the indexes of the relevant field from assessments.csv.
        
        Get the indexes of the relevant field from assessments.csv.
        The indexes are stored in the `indexes` variable.

        Parameters
        ----------
        list_line : list of str
            List of every field of the assessments.csv.
        indexes : list of int
            List of indexes of every relevant field from assessments.csv.
            Follow the format 
            [code_module_index, code_presentation_index, 
             id_assessment_index, assessment_type_index]

        Returns
        -------
        None.

        """
        for count_field, field in enumerate(list_line):
            if field == "code_module":
                indexes[0] = count_field
            elif field == "code_presentation":
                indexes[1] = count_field
            elif field == "id_assessment":
                indexes[2] = count_field
            elif field == "assessment_type":
                indexes[3] = count_field
        return indexes
                
    def getCoursesIndexes(self, list_line, indexes):
        """Get the indexes of the relevant field from courses.csv.
        
        Get the indexes of the relevant field from courses.csv.
        The indexes are stored in the `indexes` variable.

        Parameters
        ----------
        list_line : list of str
            List of every field of the courses.csv.
        indexes : list of int
            List of indexes of every relevant field from courses.csv.
            Follow the format 
            [code_module_index, code_presentation_index,
             module_presentation_length_index]

        Returns
        -------
        None.

        """
        for count_field, field in enumerate(list_line):
            if field == "code_module":
                indexes[0] = count_field
            elif field == "code_presentation":
                indexes[1] = count_field
            elif field == "module_presentation_length":
                indexes[2] = count_field
        return indexes

    def addStudentInfoValues(self, list_line, indexes):
        """Add the relevant values from studentInfo.csv to the `final_results`
        list.
        
        Add the relevant values from studentInfo.csv to the `final_results`
        list. Check before if the indexes are not null, else the indexes
        which are null are specified.

        Parameters
        ----------
        list_line : list of str
            List of every field of the studentInfo.csv.
        indexes : list of int
            List of indexes of every relevant field from studentInfo.csv.
            Follow the format 
            [code_module_index, code_presentation_index,
             id_student_index, final_result_index]

        Returns
        -------
        None.

        """
        code_module_index = indexes[0]
        code_presentation_index = indexes[1]
        id_student_index = indexes[2]
        final_result_index = indexes[3]
        if(code_module_index != None and code_presentation_index != None
           and id_student_index != None and final_result_index != None):
            code_module = list_line[code_module_index]
            code_presentation = list_line[code_presentation_index]
            id_student = list_line[id_student_index]
            final_result = list_line[final_result_index]
            self.final_results.append([code_module, code_presentation,
                                       id_student, final_result])
        else:
            if(code_module_index == None):
                print("Missing code_module index.")
            if(code_presentation_index == None):
                print("Missing code_presentation index.")
            if(id_student_index == None):
                print("Missing id_student index.")
            if(final_result_index == None):
                print("Missing final_result index.")
                
    def addStudentAssessmentValues(self, list_line, indexes):
        """Add the relevant values from studentAssessment.csv to the 
        `scores` list.
        
        Add the relevant values from studentAssessment.csv to the 
        `scores` list. Check before if the indexes are not null, 
        else the indexes which are null are specified.

        Parameters
        ----------
        list_line : list of str
            List of every field of the studentAssessment.csv.
        indexes : list of int
            List of indexes of every relevant field from studentAssessment.csv.
            Follow the format 
            [id_assessment_index, id_student_index, score_index]

        Returns
        -------
        None.

        """
        id_assessment_index = indexes[0]
        id_student_index = indexes[1]
        score_index = indexes[2]
        if(id_assessment_index != None and id_student_index != None and score_index != None):
            id_assessment = list_line[id_assessment_index]
            id_student = list_line[id_student_index]
            score = list_line[score_index]
            # Some "" exist, so to avoid bugs, those strings are replaced by "0".
            if score == "": 
                score = "0"
            self.scores.append([id_assessment, id_student, score])
        else:
            if(id_assessment_index == None):
                print("Missing id_assessment index.")
            if(id_student_index == None):
                print("Missing id_student index.")
            if(score_index == None):
                print("Missing score index.")
                
    def addAssessmentsValues(self, list_line, indexes):
        """Add the relevant values from assessments.csv to the 
        `assessments_types` list.
        
        Add the relevant values from assessments.csv to the `assessments_types`
        list. Check before if the indexes are not null, 
        else the indexes which are null are specified.

        Parameters
        ----------
        list_line : list of str
            List of every field of the assessments.csv.
        indexes : list of int
            List of indexes of every relevant field from assessments.csv.
            Follow the format 
            [code_module_index, code_presentation_index,
             id_assessment_index, assessment_type_index]

        Returns
        -------
        None.

        """
        code_module_index = indexes[0]
        code_presentation_index = indexes[1]
        id_assessment_index = indexes[2]
        assessment_type_index = indexes[3]
        if(code_module_index != None and code_presentation_index != None and id_assessment_index != None and assessment_type_index != None):
            code_module = list_line[code_module_index]
            code_presentation = list_line[code_presentation_index]
            id_assessment = list_line[id_assessment_index]
            assessment_type = list_line[assessment_type_index]
            self.assessments_types.append([code_module, code_presentation, id_assessment, assessment_type])
        else:
            if(code_module_index == None):
                print("Missing code_module index.")
            if(code_presentation_index == None):
                print("Missing code_presentation index.")
            if(id_assessment_index == None):
                print("Missing id_assessment index.")
            if(assessment_type_index == None):
                print("Missing assessment_type index.")
                
    def addCoursesValues(self, list_line, indexes):
        """Add the relevant values from courses.csv to the 
        `modules_presentations_lengths` list.
        
        Add the relevant values from courses.csv to the 
        `modules_presentations_lengths` list. 
        Check before if the indexes are not null, 
        else the indexes which are null are specified.

        Parameters
        ----------
        list_line : list of str
            List of every field of the courses.csv.
        indexes : list of int
            List of indexes of every relevant field from courses.csv.
            Follow the format 
            [code_module_index, code_presentation_index,
             module_presentation_length_index]

        Returns
        -------
        None.

        """
        code_module_index = indexes[0]
        code_presentation_index = indexes[1]
        module_presentation_length_index = indexes[2]
        if(code_module_index != None  and code_presentation_index != None 
           and module_presentation_length_index != None):
            code_module = list_line[code_module_index]
            code_presentation = list_line[code_presentation_index]
            module_presentation_length = list_line[module_presentation_length_index]
            self.modules_presentations_lengths.append([code_module, code_presentation, 
                                                       module_presentation_length])
        else:
            if(code_module_index == None):
                print("Missing code_module index.")
            if(code_presentation_index == None):
                print("Missing code_presentation index.")
            if(module_presentation_length_index == None):
                print("Missing module_presentation_length index.")