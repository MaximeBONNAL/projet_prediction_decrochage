import torch
import torch.nn as nn
import torch.nn.functional as F
from codecarbon import EmissionsTracker
from sklearn.ensemble import RandomForestClassifier
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from collections import Counter
from sklearn.metrics import confusion_matrix

import xgboost as xgb
from sklearn.metrics import accuracy_score
from sklearn import preprocessing


import os

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"


class NeuralNetwork(nn.Module):
    """
    Architecture of the neural network.
    Inherit from torch.nn.Module

    Attributes
    ----------
    hidden_layers : list of nn.Linear
        List of the hidden layers of the neural network.

    input : nn.Linear
        Input layer of the neural network.

    output : nn.Linear
        Output layer of the neural network.
    """

    def __init__(self, input_size=20003, output_size=2, hidden_layers_sizes=[1000, 500, 250, 125]):
        """Constructor of the NeuralNetwork class

        Constructor of the NeuralNetwork class which has huffle`input_size`,
        `output_size` and `hidden_layers_sizes` has optional parameters.

        Parameters
        ----------
        input_size : int, optional
            The size of the input layer. The default is 62.
        output_size : int, optional
            The size of the output layer. The default is 2.
        hidden_layers_sizes : list of int, optional
            The size of each hidden layer. The default is [25,25,10].

        Asserts
        -------
        hidden_layers_sizes must be a list.

        Returns
        -------
        None.

        """
        super(NeuralNetwork, self).__init__()

        assert isinstance(hidden_layers_sizes, list), "The parameter size_hidden_layers should be a list"
        nb_hidden_layers = len(hidden_layers_sizes)
        self.hidden_layers = []
        self.input = nn.Linear(input_size, hidden_layers_sizes[0], device='cuda:0')

        if nb_hidden_layers > 1:
            for index_layer in range(nb_hidden_layers - 1):
                self.hidden_layers.append(
                    nn.Linear(hidden_layers_sizes[index_layer], hidden_layers_sizes[index_layer + 1], device='cuda:0'))

        self.output = nn.Linear(hidden_layers_sizes[-1], output_size, device='cuda:0')

    def forward(self, x):
        """Forward propagation of the neural network.

        Forward the input data through the network and return the output data.

        Parameters
        ----------
        x : torch.Tensor
            Input data.

        Returns
        -------
        x : torch.Tensor
            Output data.

        """
        x = x.to(device)
        x = self.input(x)
        x = F.relu(x)

        for layer in self.hidden_layers:
            x = layer(x)
            #x = F.dropout(x)
            x = F.relu(x)
            #x = torch.nn.BatchNorm1d(100)
            x = F.dropout(x)
            x = F.relu(x)

        x = self.output(x)
        x = F.softmax(x, dim=-1)

        return x


class EncoderDecoder(nn.Module):
    def __init__(self, input_size=20003, output_size=2, hidden_layers_sizes=[1000, 500, 250, 125], latent_dim=128):
        super(EncoderDecoder, self).__init__()

        # Encoder layers
        self.encoder_input = nn.Linear(input_size, hidden_layers_sizes[0])
        self.encoder_hidden = nn.ModuleList()
        for i in range(len(hidden_layers_sizes) - 1):
            layer = nn.Sequential(
                nn.Linear(hidden_layers_sizes[i], hidden_layers_sizes[i + 1]),
                nn.ReLU(),
                nn.Dropout(0.4)
            )
            self.encoder_hidden.append(layer)
        self.latent_layer = nn.Linear(hidden_layers_sizes[-1], latent_dim)

        # Decoder layers
        self.decoder_input = nn.Linear(latent_dim, hidden_layers_sizes[-1])
        self.decoder_hidden = nn.ModuleList()
        for i in range(len(hidden_layers_sizes) - 1, 0, -1):
            layer = nn.Sequential(
                nn.Linear(hidden_layers_sizes[i], hidden_layers_sizes[i - 1]),
                nn.ReLU(),
                nn.Dropout(0.4)
            )
            self.decoder_hidden.append(layer)
        self.output_layer = nn.Linear(hidden_layers_sizes[0], output_size)

    # def forward(self, x):
    #     # Encode
    #     x_enc = self.encoder_input(x)
    #     for layer in self.encoder_hidden:
    #         x_enc = layer(x_enc)
    #     latent_representation = self.latent_layer(x_enc)
    #
    #     # Decode
    #     x_dec = self.decoder_input(latent_representation)
    #     for layer in self.decoder_hidden:
    #         x_dec = layer(x_dec)
    #     reconstructed_data = self.output_layer(x_dec)
    #
    #     return reconstructed_data, latent_representation

    def encode(self, x):
        x_enc = self.encoder_input(x)
        for layer in self.encoder_hidden:
            x_enc = layer(x_enc)
        latent_representation = self.latent_layer(x_enc)
        return latent_representation

    def decode(self, x):
        x_dec = self.decoder_input(x)
        for layer in self.decoder_hidden:
            x_dec = layer(x_dec)
        reconstructed_data = self.output_layer(x_dec)
        return reconstructed_data

class ConvertedData(Dataset):
    """
    Creation of a Dataset usable by a DataLoader.
    Inherit from torch.utils.data.Dataset

    Attributes
    ----------
    data : list of list of float
        Converted data as a list.

    label : list of int
        Converted label as a list.

    class_weights : torch.Tensor
        Weights of each class in label.
    """

    def __init__(self, data, label):
        """Constructor of the ConvertedData class.

        Constructor of the ConvertedData class which takes the input `data`
        and the `label` to make it usable by a DataLoader.


        Parameters
        ----------
        data : list of list of float
            Converted data as a list.
        label : list of int
            Converted label as a list.

        Returns
        -------
        None.

        """
        self.data = np.array(data)
        self.label = label
        self.counter_label = Counter(self.label)
        count_label = [self.counter_label[0], self.counter_label[1]]
        print("c ", count_label)
        self.class_weights = torch.from_numpy(len(self.label) / (np.array(count_label))).type(torch.float).to(device)


    def __len__(self):
        """Get length of `data`
        Returns
        -------
        int
            Length of `data`.

        """
        return len(self.data)

    def __getitem__(self, index):
        """Get item at index.

        Get `data` and `label` at `index` as tensors.


        Parameters
        ----------
        index : int
            Index of the data and label to get.

        Returns
        -------
        tensor_data : torch.Tensor
            Tensor corresponding to the data at index.
        tensor_label : torch.Tensor
            Tensor corresponding to the data at label.

        """
        tensor_data = torch.tensor(self.data[index])
        np_label = np.zeros(2)
        np_label[self.label[index]] += 1
        tensor_label = torch.tensor(np_label)
        return tensor_data, tensor_label

def ProcessData(csv_file_list, output_path_mergedata, output_path_new_data ,CHUNK_SIZE = 200000):
    """
    Lecture et ecriture dans un nouveau fichier csv des données train_log et test_log traitées.
    On obtient un jeu de données où chaque ligne correspond à toutes les activités d'un utilisateur.

    Parameters
    ----------
    csv_file_list
    output_path_mergedata
    output_path_new_data
    CHUNK_SIZE

    Returns
    -------

    """
    usecols = ["enroll_id", "username", "course_id", "action", "time"]
    columns = ["course_id", "action", 'time']
    le = preprocessing.LabelEncoder()
    dataset_opened = pd.DataFrame(dtype='int')
    compteur = 0
    # Lecture et ecriture de train_log et test_log dans un fichier commun
    for csv_file_name in csv_file_list:
        print(csv_file_name)
        chunk_container = pd.read_csv(csv_file_name, usecols=usecols, chunksize=CHUNK_SIZE, dtype='string')
        for chunk in chunk_container:
            print(compteur)
            compteur += 1
            # conversion date en int
            date_as_int = []
            for i in range(len(chunk)):
                year = pd.to_datetime(chunk.iloc[i]['time']).year
                month = pd.to_datetime(chunk.iloc[i]['time']).month
                day = pd.to_datetime(chunk.iloc[i]['time']).day
                hour = pd.to_datetime(chunk.iloc[i]['time']).hour
                minute = pd.to_datetime(chunk.iloc[i]['time']).minute
                second = pd.to_datetime(chunk.iloc[i]['time']).second
                date_as_int.append(
                    year * 10000000000 + month * 100000000 + day * 1000000 + hour * 10000 + minute * 100 + second)
            # transforme en int les valeurs string
            for c in columns:
                le.fit(chunk[c])
                chunk[c] = le.transform(chunk[c])
            chunk = chunk.astype(dtype='int')
            # application de la conversion
            chunk['time'] = date_as_int
            print('concat')
            dataset_opened = pd.concat([dataset_opened, chunk])
        print('tocsv')
        dataset_opened.to_csv(output_path_mergedata, index=False)
    print(dataset_opened.head())
    print("lecture finito")

    ## Groupement des données pour avoir une ligne par id
    chunk_container = pd.read_csv(output_path_mergedata, chunksize=CHUNK_SIZE, dtype='float')
    save = [None]
    save_index = 0
    for chunk in chunk_container:
        e_id = chunk.iloc[0]['enroll_id']
        dataset = [[None] * 20000] # grand nombre arbitraire en raison d'users avec plus de 19000 activités. Reduction possible en ignorants une partie des données de ces etudiants
        j = 0
        dataset[0][0] = chunk.iloc[0]['enroll_id']
        dataset[0][1] = chunk.iloc[0]['username']
        k = 2
        while k < len(dataset[0]) - 1:
            for i in range(len(chunk) - 1):
                if save != [None] and save[0] == e_id:
                    for l in range(len(save) - 1):
                        dataset[j][l] = save[l]
                    k = save_index
                    save = [None]
                    save_index = 0
                if save != [None] and save[0] != e_id:
                    for l in range(len(save) - 1):
                        dataset[j][l] = save[l]
                    save_index = 0
                    save = [None]
                    j += 1
                    dataset += [[None] * 20000]
                    dataset[j][0] = chunk.iloc[0]['enroll_id']
                    dataset[j][1] = chunk.iloc[0]['username']
                    k = 2
                if chunk.iloc[i]['enroll_id'] == e_id:
                    if k < len(dataset[j]):
                        dataset[j][k] = chunk.iloc[i]['course_id']
                        dataset[j][(k + 1)] = chunk.iloc[i]['action']
                        dataset[j][(k + 2)] = chunk.iloc[i]['time']
                        if k > 15000:
                            print(k)
                        k += 3
                else:
                    e_id = chunk.iloc[i]['enroll_id']
                    dataset += [[None] * 20000]
                    j += 1
                    k = 0
                    print(j)
                    dataset[j][k] = chunk.iloc[i]['enroll_id']
                    dataset[j][k + 1] = chunk.iloc[i]['username']
                    dataset[j][k + 2] = chunk.iloc[i]['course_id']
                    dataset[j][k + 3] = chunk.iloc[i]['action']
                    dataset[j][k + 4] = chunk.iloc[i]['time']
                    k += 5

                if i == len(chunk) - 2:
                    save = dataset[j]
                    save_index = k
                    dataset[j] = [None]
            break
        print("writing")
        pd.DataFrame(dataset[:j]).to_csv(output_path_new_data, mode="a", header=False, index=False, sep=',')

    dataset = [[None] * 20000]
    for l in range(len(save) - 1):
        dataset[0][l] = save[l]
    pd.DataFrame(dataset).to_csv(output_path_new_data, mode="a", header=False, index=False, sep=',')

def ProportionedDataset(data, data_truth, output_path_truth, output_path_data, ratio=1.25):
    """
    Création d'un dataset rééquilibrant le nombre d'étudiants par label

    Parameters
    ----------
    data
    data_truth
    output_path_truth
    output_path_data
    ratio

    Returns
    -------

    """
    # data_truth = pd.read_csv(data_truth, index_col='enroll_id')
    # data = pd.read_csv(data, header=None, dtype='float')

    ## Rééquilibrage des classes
    vrai_train = data_truth[data_truth['truth'] == 0]
    faux_train = data_truth[data_truth['truth'] == 1]
    count_vrai_train = len(vrai_train)
    count_faux_train = len(faux_train)
    print("vrai_train ", count_vrai_train)
    print("faux_train ", count_faux_train)
    target_count = min(count_vrai_train, count_faux_train)
    target_count_train_faux = int(target_count * ratio)

    classe_1_train = vrai_train.sample(n=target_count, random_state=42)
    classe_2_train = faux_train.sample(n=target_count_train_faux, random_state=42)

    balanced_df_train = pd.concat([classe_1_train, classe_2_train])
    balanced_df_train = balanced_df_train.sort_index()
    label_truth_train = np.array(balanced_df_train['truth'].values)
    print("len bdf", len(balanced_df_train))
    balanced_df_train.reset_index()
    data_ratioed = [[]]

    ## Creation dataset rééquilibré des labels
    k = 0
    while k < (len(label_truth_train) - 2):
        for i in range(len(label_truth_train) - 1):
            if k == len(data) - 1:
                break
            print(balanced_df_train.index[i], data.iloc[k, 0])
            if balanced_df_train.index[i] > data.iloc[k, 0]:
                for j in range(k + 1, len(data) - 1):
                    print(balanced_df_train.index[i], data.iloc[j, 0])
                    if balanced_df_train.index[i] <= data.iloc[j, 0]:
                        k = j
                        break
            if balanced_df_train.index[i] == data.iloc[k, 0]:
                print(balanced_df_train['truth'].iloc[i])
                data_ratioed += [[balanced_df_train.index[i], balanced_df_train['truth'].iloc[i]]]
                k += 1
        break
    del data_ratioed[0]
    print("writing")
    data_truth = pd.DataFrame(np.array(data_ratioed))
    data_truth.columns = ['enroll_id', 'truth']
    data_truth.to_csv(output_path_truth, header=['enroll_id', 'truth'], index=None, sep=',')

    data_ratioed = [[]]

    ## Creation dataset rééquilibré
    k = 0
    while k < len(balanced_df_train) - 2:
        for i in range(len(data) - 2):
            print(data.iloc[i, 0], balanced_df_train.index[k])
            if data.iloc[i, 0] > balanced_df_train.index[k]:
                for j in range(k, len(balanced_df_train) - 1):
                    print(data.iloc[i, 0], balanced_df_train.index[k])
                    if data.iloc[i, 0] <= balanced_df_train.index[j]:
                        k = j
                        break
            if data.iloc[i, 0] == balanced_df_train.index[k]:
                data_ratioed += [data.iloc[i, :]]
                print('k', k)
                k += 1
        break
    del data_ratioed[0]
    print("writing")
    new_data = pd.DataFrame(data_ratioed)
    new_data.to_csv(output_path_data, header=False, index=False, sep=',')

def FindUsersInfo(train_truth, test_truth, outpout_path_train, outpout_path_test):
    """
    Creation d'un dataset des informations personnelles des étudiants d'un dataset en recherchant dans le fichier 'user_info.csv'

    Parameters
    ----------
    train_truth
    test_truth
    outpout_path_train
    outpout_path_test

    Returns
    -------

    """
    train_truth = pd.read_csv(train_truth)
    test_truth = pd.read_csv(test_truth)
    data_truth = pd.concat([train_truth, test_truth])
    print('users')
    users = pd.read_csv("../prediction_log/user_info.csv", dtype='string',
                        index_col='user_id').fillna('0')
    users = users.sort_index()

    ## Encodage des valeurs textuelles
    columns = ['gender', 'education']
    le = preprocessing.LabelEncoder()
    for c in columns:
        le.fit(users[c])
        users[c] = le.transform(users[c])
    users = users.astype(dtype='float')

    ## Recuperation des info des utilisateurs seulement pour les id concernes
    train_users_ratioed = pd.DataFrame(columns=['user_id', 'gender', 'education', 'birth'])
    test_users_ratioed = pd.DataFrame(columns=['user_id', 'gender', 'education', 'birth'])
    for i in range(len(users) - 1):
        if data_truth['enroll_id'].isin([int(users.index[i])]).any() == True:
            print(users.index[i])
            new_col = pd.DataFrame({'user_id': [users.index[i]], 'gender': [users['gender'].iloc[i]],
                                    'education': [users['education'].iloc[i]],
                                    'birth': [users['birth'].iloc[i]]})
            if train_truth['enroll_id'].isin([int(users.index[i])]).any() == True:
                train_users_ratioed = pd.concat([train_users_ratioed, new_col], ignore_index=True)
            else:
                test_users_ratioed = pd.concat([test_users_ratioed, new_col], ignore_index=True)

    train_users_ratioed.to_csv(outpout_path_train, header=True, index=False, sep=',')
    test_users_ratioed.to_csv(outpout_path_test, header=True, index=False, sep=',')

def AddUserInfo(users_info, data, data_truth, output_path):
    """
    Ajout des informations personnelles des etudiants au dataset

    Parameters
    ----------
    users_info
    data
    data_truth
    output_path

    Returns
    -------

    """
    data = pd.read_csv(data, header=None, dtype='float')
    data_truth = pd.read_csv(data_truth, index_col='enroll_id')
    users_info = pd.read_csv(users_info, index_col='user_id')
    users_info = users_info.sort_index()
    gender_ratioed = [[]]
    education_ratioed = [[]]
    birth_ratioed = [[]]
    k = 0
    while k < (len(data_truth)):
        for i in range(len(users_info)):
            if k == len(data_truth):
                break
            print(users_info.index[i], data_truth.iloc[k, 0])
            if users_info.index[i] > data_truth.iloc[k, 0]:
                gender_ratioed += [0.0]
                education_ratioed += [0.0]
                birth_ratioed += [0.0]
                if data_truth['enroll_id'].isin([int(users_info.index[i])]).any() == True:
                    for j in range(k + 1, len(data_truth) - 1):
                        print(users_info.index[i], data_truth.iloc[j, 0])
                        if users_info.index[i] <= data_truth.iloc[j, 0]:
                            k = j
                            break
                        else:
                            gender_ratioed += [0.0]
                            education_ratioed += [0.0]
                            birth_ratioed += [0.0]

            if users_info.index[i] == data_truth.iloc[k, 0]:
                print(users_info.index[i])
                gender_ratioed += [users_info['gender'].iloc[i]]
                education_ratioed += [users_info['education'].iloc[i]]
                birth_ratioed += [users_info['birth'].iloc[i]]
                print('k', k)
                k += 1
        break

    ## Suppression du header
    del gender_ratioed[0]
    del education_ratioed[0]
    del birth_ratioed[0]

    # Ajout dans le dataset
    data.insert(1, 'gender', gender_ratioed[0])
    data.insert(2, 'education', education_ratioed[0])
    data.insert(3, 'birth', birth_ratioed[0])
    print('write')
    data.to_csv(output_path, header=False, index=False, sep=',')

def CreateDataset():
    """
    Instancie la creation du dataset 'processed_fulldata_train_merge_ratioed_4060+Usersinfo.csv' trouvable sur le repository.
    La création du dataset peut prendre jusqu'a 15 heures. toutes les méthodes étapes de création sont presentent, neanmoins on risque un dépassement
    memoire en raison du trop grand nombre de donnees dans l'état actuel.
    Prévilégiez de créer les différents fichiers csv indépendemment pour éviter cela.
    Necessite de modifier les chemins d'accès aux fichiers et de téléchager les donnees "Dropout Prediction Dataset" à l'url 'http://moocdata.cn/data/user-activity#User%20Activity'

    Returns
    -------

    """
    csv_file_list = ["../prediction_log/train_log.csv",
                     "../prediction_log/test_log.csv"]
    # Lecture des datasets, traitement et écriture du nouveaux jeu de données
    ProcessData(csv_file_list=csv_file_list, output_path_mergedata="rep/fulldata.csv",
                output_path_new_data="rep/fulldata_processed.csv")

    ####Creation du dataset re proportionné

    # lignes 0 à 157943 étudiants de train_log
    # Lecture du nombre maximum d'étudiants avant le dépassement de mémoire
    train_data = pd.read_csv("rep/fulldata_processed.csv", header=None, dtype='float')
    train_truth = pd.read_csv("../prediction_log/train_truth.csv", index_col='enroll_id')
    # Creation d'un dataset de train reproportionne
    ProportionedDataset(data=train_data, data_truth=train_truth, output_path_truth='rep/prop_train_truth.csv',
                        output_path_data='rep/prop_train.csv')

    # Lecture partie Test
    test_data = pd.read_csv("rep/fulldata_processed.csv", header=None, dtype='float', skiprows=5)
    test_truth = pd.read_csv("../prediction_log/test_truth.csv", index_col='enroll_id')
    # Creation d'un dataset de test reproportionne
    ProportionedDataset(data=test_data, data_truth=test_truth, output_path_truth='rep/prop_test_truth.csv',
                        output_path_data='rep/prop_test.csv')

    # Recherche des informations des utilisateurs present dans les datasets
    FindUsersInfo(train_truth='rep/prop_train_truth.csv', test_truth='rep/prop_test_truth.csv',
                  outpout_path_train='rep/train_users_info.csv', outpout_path_test="rep/test_users_info.csv")
    # Ajout des donnees personnelles des utilisateurs aux datasets
    AddUserInfo(users_info='rep/train_users_info.csv', data='rep/prop_train.csv', data_truth='rep/prop_train_truth.csv',
                output_path='rep/prop_train+users_info.csv')
    AddUserInfo(users_info='rep/test_users_info.csv', data='rep/prop_test.csv', data_truth='rep/prop_test_truth.csv',
                output_path='rep/prop_test+users_info.csv')


if __name__ == "__main__":
    if torch.cuda.is_available():
        dev = "cuda:0"
    else:
        dev = "cpu"

    device = torch.device(dev)
    print(torch.cuda.current_device())

    # Defining the ratio of train, valid and test dataset
    train_ratio = 0.65
    valid_ratio = 0.15
    test_ratio = 0.2

    ## Préviligiez l'utilisation d'un dataset existant, la création est très longue et potentiellement non fonctionnelle en l'état.
    #CreateDataset()

    data = pd.read_csv("datasets/processed_fulldata_train_merge_ratioed_4060+Usersinfo.csv", header=None, dtype=float).fillna(0)
    data_truth = pd.read_csv("train_truth_4060.csv", index_col='enroll_id')
    label_truth = np.array(data_truth['truth'])

    len_data = len(data)
    nb_train_data = int(len_data * train_ratio)
    nb_valid_data = int(len_data * valid_ratio)

    train_data = data[:nb_train_data]
    train_label = label_truth[:nb_train_data]
    train_dataset = ConvertedData(train_data, np.array(train_label))
    print("len train_dataset: ", len(train_dataset))

    valid_data = data[nb_train_data:(nb_train_data + nb_valid_data)]
    valid_label = label_truth[nb_train_data:(nb_train_data + nb_valid_data)]
    valid_dataset = ConvertedData(valid_data, np.array(valid_label))
    print("len valid_dataset:", len(valid_dataset))

    test_data = data[(nb_train_data + nb_valid_data):(len(data) - 1)]
    test_label = label_truth[(nb_train_data + nb_valid_data):len(data) - 1]
    test_dataset = ConvertedData(test_data, np.array(test_label))
    print("len test_dataset", len(test_dataset))

    # # Getting each Dataloader.
    batch_size = 1000
    train_dataloader = DataLoader(train_dataset, shuffle=False, batch_size=batch_size)
    valid_dataloader = DataLoader(valid_dataset, shuffle=False, batch_size=batch_size)
    test_dataloader = DataLoader(test_dataset, shuffle=True, batch_size=batch_size)

    ## XGBRF
    print('XGBRF')
    XGBRF_train_data = pd.concat([train_data, valid_data])
    XGBRF_train_label = np.hstack([train_label, valid_label])

    model_xgb = xgb.XGBRFClassifier(n_estimators=1500, subsample=0.1, colsample_bynode=0.1)
    model_xgb.fit(XGBRF_train_data, XGBRF_train_label)
    xgb_pred = model_xgb.predict(test_data)
    predictions = [round(value) for value in xgb_pred]
    # evaluate predictions
    accuracy = accuracy_score(test_label, predictions)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))

    pred_list = predictions
    out_df = pd.DataFrame()
    out_df['ID'] = ["Pass/Distinction", "Withdrawn/Fail"]
    count_pred = Counter(pred_list)
    out_df['Pred'] = np.array([count_pred[0], count_pred[1]])
    count_label = Counter(test_label)
    out_df['Label'] = np.array([count_label[0], count_label[1]])
    print(out_df.head())

    conf_matrix = confusion_matrix(test_label, pred_list)
    row_labels = ["Real Pass/Distinction", "Real Withdrawn/Fail"]
    columns_labels = ["Predicted Pass/Distinction", "Predicted Withdrawn/Fail"]
    conf_df = pd.DataFrame(conf_matrix, columns=columns_labels, index=row_labels)
    print(conf_df)
    tn, fp, fn, tp = conf_matrix.ravel()
    p = tp + fn
    n = fp + tn
    pp = tp + fp
    pn = fn + tn
    tpr = tp / p
    tnr = tn / n
    for_ = fn / pn
    fdr = fp / pp
    acc_test = (tp + tn) / (p + n) * 100
    print("Accuracy on the test dataset : ", acc_test)
    print("True positive rate :", tpr)
    print("True negative rate :", tnr)
    print("False omission rate :", for_)
    print("False discovery rate :", fdr)

    ## DecoderEncoder
    encoder_decoder = EncoderDecoder()
    encoder_decoder = encoder_decoder.to(device)
    CELoss = torch.nn.CrossEntropyLoss(weight=train_dataset.class_weights, reduction='mean')
    optimizer = torch.optim.Adam(encoder_decoder.parameters(), lr=0.001, betas=(0.9, 0.999))

    # #Initializing training variables.
    mean_train_losses = []
    mean_valid_losses = []
    valid_acc_list = []
    epochs = 20
    best_acc = 0
    epoch_best_acc = 0

    for epoch in range(epochs):
        encoder_decoder.train()

        train_losses = []
        valid_losses = []

        for batch, (data, label) in enumerate(train_dataloader):
            data = data.type(torch.float32).to(device)
            label = label.type(torch.float32).to(device)

            optimizer.zero_grad()

            # Passer les données par l'encodeur pour obtenir la représentation latente
            latent_representation = encoder_decoder.encode(data)

            # Passer la représentation latente par le décodeur pour obtenir la reconstruction
            reconstructed_data = encoder_decoder.decode(latent_representation)

            loss = CELoss(reconstructed_data, label)
            loss.backward()
            optimizer.step()

            train_losses.append(loss.item())
            if (batch * batch_size) % (batch_size * 100) == 0:
                print(f'{batch * batch_size} / {len(data)}')
        encoder_decoder.eval()
        correct = 0
        total = 0
        with torch.no_grad():
            for batch, (data, label) in enumerate(valid_dataloader):
                data = data.type(torch.float32).to(device)
                label = label.type(torch.float32).to(device)

                latent_representation = encoder_decoder.encode(data)
                reconstructed_data = encoder_decoder.decode(latent_representation)
                loss = CELoss(reconstructed_data, label)

                valid_losses.append(loss.item())

                for index in range(len(reconstructed_data)):
                    if torch.argmax(reconstructed_data[index]) == torch.argmax(label[index]):
                        correct += 1
                total += label.size(0)

        mean_train_losses.append(np.mean(train_losses))
        mean_valid_losses.append(np.mean(valid_losses))

        print("correct ", correct, " total ", total)
        accuracy = 100 * correct / total
        valid_acc_list.append(accuracy)
        print('epoch : {}, train loss : {:.4f}, valid loss : {:.4f}, valid acc : {:.2f}%' \
                .format(epoch + 1, np.mean(train_losses), np.mean(valid_losses), accuracy))

        if accuracy > best_acc:
            best_acc = accuracy
            index_best_acc = epoch
            torch.save(encoder_decoder, "best_model.pt")

    print("Best model on the validation dataset at epoch " + str(epoch) + " with " + str(best_acc) + "% accuracy.")

    # # Evaluate model on test dataset
    print('testing')
    data = pd.read_csv("datasets/processed_fulldata_test_merge_ratioed_4060+Usersinfo.csv", header=None, dtype='float').fillna(0)
    test_truth = pd.read_csv("fulldata_test_truth_merge_4060.csv")
    test_label = np.array(test_truth['truth'])
    test_dataset = ConvertedData(data, np.array(test_label))
    print("len tl", len(test_dataset))
    test_dataloader = DataLoader(test_dataset, shuffle=True, batch_size=batch_size)

    model = torch.load("best_model.pt")
    model.eval()
    test_preds = torch.LongTensor().to(device)
    output_test_features = []
    for batch, (data, label) in enumerate(test_dataloader):
        data = data.type(torch.float32).to(device)
        label = label.type(torch.float32).to(device)

        # Obtenir la représentation latente à partir des données de test
        latent_representation = model.encode(data)

        # Obtenir les prédictions reconstruites à partir de la représentation latente
        reconstructed_data = model.decode(latent_representation)
        pred = reconstructed_data.max(1, keepdim=True)[1]
        test_preds = torch.cat((test_preds, pred), dim=0)
    pred_list = test_preds.squeeze()

    out_df = pd.DataFrame()
    out_df['ID'] = ["Pass/Distinction", "Withdrawn/Fail"]
    count_pred = Counter(pred_list.cpu().detach().numpy())
    out_df['Pred'] = np.array([count_pred[0], count_pred[1]])
    count_label = Counter(test_label)
    out_df['Label'] = np.array([count_label[0], count_label[1]])
    print(out_df.head())

    conf_matrix = confusion_matrix(test_label, pred_list.cpu().numpy())
    row_labels = ["Real Pass/Distinction", "Real Withdrawn/Fail"]
    columns_labels = ["Predicted Pass/Distinction", "Predicted Withdrawn/Fail"]
    conf_df = pd.DataFrame(conf_matrix, columns=columns_labels, index=row_labels)
    print(conf_df)
    tn, fp, fn, tp = conf_matrix.ravel()
    p = tp + fn
    n = fp + tn
    pp = tp + fp
    pn = fn + tn
    tpr = tp / p
    tnr = tn / n
    for_ = fn / pn
    fdr = fp / pp
    acc_test = (tp + tn) / (p + n) * 100
    print("Accuracy on the test dataset : ", acc_test)
    print("True positive rate :", tpr)
    print("True negative rate :", tnr)
    print("False omission rate :", for_)
    print("False discovery rate :", fdr)



