# Projet de prediction des élèves en situation de décrochage


## Objectifs du projet
Mise en place d’une méthode de prédiction des élèves en situation de décrochage au
moyen de techniques d’apprentissage automatique comme des réseaux de neurones ou des méthodes de gradient boosting.
Ce projet est hérité de https://gitlab.com/Hugo_LB/prd avec pour objectif d'améliorer les résultats et explorer de nouvelles méthodes de classification.


## Versions et installations
L'ensemble du projet à été réalisé avec les versions suivantes :
- Python 3.9.0, suivre le détail de l'installation depuis : https://www.python.org/downloads/release/python-390/
- PyTorch 1.10.1, suivre le détail de l'installation depuis : https://pytorch.org/get-started/previous-versions/
- Matplotlib 3.3.4, suivre le détail de l'installation depuis : https://matplotlib.org/stable/users/installing/index.html
- NumPy 1.20.1, suivre le détail de l'installation depuis : https://numpy.org/install/
- Pandas 1.2.4, suivre le détail de l'installation depuis : https://pandas.pydata.org/docs/getting_started/install.html
- scikit-learn 0.24.1, suivre le détail de l'installation depuis : https://scikit-learn.org/stable/install.html
- XGBoost 2.0.0, suivre le détail de l'installation depuis : https://xgboost.readthedocs.io/en/stable/install.html
- Catboost 1.2, suivre le détail de l'installation depuis : https://catboost.ai/en/docs/installation/python-installation-method-pip-install
- LightGBM 3.3.5, suivre le détail de l'installation depuis : https://lightgbm.readthedocs.io/en/latest/Python-Intro.html


## Description fichiers

- Le fichier AttentionNetwork.py explore les algorithmes Catboost, LightGBM, XGBoost, XGBoostRandomForest, sk-learn RandomForest et un réseau de neurones avec couches d'attention sur un lot de colonnes choisies.
- Le fichier XuetangxNeuralNetwork utilise les datasets de l'archive 'datasets_xuetangx.zip', issue d'un traitement des données de http://moocdata.cn/data/user-activity#User%20Activity et explore les méthodes Encodeur-Decodeur et XGBoostRandomForest, qui est largement plus efficace.